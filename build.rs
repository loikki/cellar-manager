use std::env;

fn main() {
    // Tauri feature does not work properly => use derived variable
    match env::var("TAURI_ANDROID_PROJECT_PATH") {
        Ok(_) => tauri_build::build(),
        _ => {}
    }
}
