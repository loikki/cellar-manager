CREATE TABLE category (
  id SERIAL PRIMARY KEY,
  name varchar(255) NOT NULL,
  UNIQUE(name)
);

CREATE INDEX idx_name ON category(name);

CREATE TABLE item (
  id SERIAL PRIMARY KEY,
  name varchar(255) NOT NULL,
  UNIQUE(name),
  quantity INT UNSIGNED NOT NULL,
  last_modification DATETIME NOT NULL,
  category_id BIGINT UNSIGNED NOT NULL REFERENCES category(id)
);

CREATE INDEX idx_name ON item(name);
