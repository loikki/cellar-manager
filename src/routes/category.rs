use rocket::serde::json::Json;
use rocket::tokio::sync::Mutex;
use rocket::{delete, get, put, routes, Route, State};

use crate::category::Category;
use crate::cellar_error::CellarError;
use crate::config::Config;
#[cfg(feature = "diesel")]
use crate::database::Database;

pub fn get_routes() -> Vec<Route> {
    routes![get_categories, add_category, delete_category]
}

#[get("/")]
pub async fn get_categories(
    config: &State<Mutex<Config>>,
) -> Result<Json<Vec<Category>>, CellarError> {
    let config = config.inner().lock().await;
    #[cfg(feature = "diesel")]
    {
        let mut db = Database::new(config.database_url());
        return Ok(Json(db.get_categories()));
    }
    return Err(CellarError::Cellar("Not implemented".to_string()));
}

#[put("/", data = "<category>")]
pub async fn add_category(
    category: Json<Category>,
    config: &State<Mutex<Config>>,
) -> Result<Json<u64>, CellarError> {
    let config = config.inner().lock().await;
    #[cfg(feature = "diesel")]
    {
        let mut db = Database::new(config.database_url());
        let category = category.into_inner();
        let id = db.add_category(&category)?;
        return Ok(Json(id));
    }
    return Err(CellarError::Cellar("Not implemented".to_string()));
}

#[delete("/<id>")]
pub async fn delete_category(id: u64, config: &State<Mutex<Config>>) -> Result<(), CellarError> {
    let config = config.inner().lock().await;
    #[cfg(feature = "diesel")]
    {
        let mut db = Database::new(config.database_url());
        return db.delete_category(id);
    }
    Err(CellarError::Cellar("not implemented".to_string()))
}
