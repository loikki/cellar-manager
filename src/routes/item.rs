use chrono::Local;
use rocket::serde::json::Json;
use rocket::tokio::sync::Mutex;
use rocket::{delete, get, put, routes, Route, State};

use crate::cellar_error::CellarError;
use crate::config::Config;
#[cfg(feature = "diesel")]
use crate::database::Database;
use crate::item::Item;

pub fn get_routes() -> Vec<Route> {
    routes![get_items, add_item, set_quantity, delete_item]
}

#[get("/")]
pub async fn get_items(config: &State<Mutex<Config>>) -> Result<Json<Vec<Item>>, CellarError> {
    let config = config.inner().lock().await;
    #[cfg(feature = "diesel")]
    {
        let mut db = Database::new(config.database_url());
        return Ok(Json(db.get_items()));
    }
    Err(CellarError::Cellar("not implemented".to_string()))
}

#[put("/", data = "<item>")]
pub async fn add_item(
    item: Json<Item>,
    config: &State<Mutex<Config>>,
) -> Result<Json<u64>, CellarError> {
    let config = config.inner().lock().await;
    #[cfg(feature = "diesel")]
    {
        let mut db = Database::new(config.database_url());
        let mut item = item.into_inner();
        item.last_modification = Some(Local::now().naive_local());
        let id = db.add_item(&item)?;
        return Ok(Json(id));
    }
    Err(CellarError::Cellar("not implemented".to_string()))
}

#[put("/<id>/quantity/<quantity>")]
pub async fn set_quantity(
    id: u64,
    quantity: u32,
    config: &State<Mutex<Config>>,
) -> Result<(), CellarError> {
    let config = config.inner().lock().await;
    #[cfg(feature = "diesel")]
    {
        let mut db = Database::new(config.database_url());
        return db.set_quantity(id, quantity);
    }
    Err(CellarError::Cellar("not implemented".to_string()))
}

#[delete("/<id>")]
pub async fn delete_item(id: u64, config: &State<Mutex<Config>>) -> Result<(), CellarError> {
    let config = config.inner().lock().await;
    #[cfg(feature = "diesel")]
    {
        let mut db = Database::new(config.database_url());
        return db.delete_item(id);
    }
    Err(CellarError::Cellar("not implemented".to_string()))
}
