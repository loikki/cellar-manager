// @generated automatically by Diesel CLI.

diesel::table! {
    category (id) {
        id -> Unsigned<Bigint>,
        #[max_length = 255]
        name -> Varchar,
    }
}

diesel::table! {
    item (id) {
        id -> Unsigned<Bigint>,
        #[max_length = 255]
        name -> Varchar,
        quantity -> Unsigned<Integer>,
        last_modification -> Datetime,
        category_id -> Unsigned<Bigint>,
    }
}

diesel::allow_tables_to_appear_in_same_query!(category, item,);
