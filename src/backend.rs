use rocket::tokio::sync::Mutex;
use rocket::{http::Method, Build, Rocket};
use rocket_cors::{AllowedOrigins, CorsOptions};
use std::net::Ipv4Addr;

#[cfg(feature = "diesel")]
use futures::executor::block_on;

use crate::config::Config;
use crate::routes::{category, item};

pub fn get_rocket() -> Rocket<Build> {
    // Config
    let config = Config::new();
    let ip = if config.expose_rocket() {
        Ipv4Addr::new(0, 0, 0, 0)
    } else {
        Ipv4Addr::new(127, 0, 0, 1)
    };
    let config: Mutex<Config> = Mutex::new(config);

    let rocket_config = rocket::Config::figment().merge(("address", ip));

    // Migrate DB
    #[cfg(feature = "diesel")]
    {
        let config = block_on(config.lock());
        if config.backend_url().is_none() {
            use crate::database::Database;
            Database::new(config.database_url()).migrate();
        }
    }

    // Cors
    let cors = CorsOptions::default()
        .allowed_origins(AllowedOrigins::all())
        .allowed_methods(
            vec![
                Method::Get,
                Method::Post,
                Method::Patch,
                Method::Put,
                Method::Delete,
            ]
            .into_iter()
            .map(From::from)
            .collect(),
        )
        .allow_credentials(true);

    rocket::custom(rocket_config)
        .manage(config)
        .mount("/item", item::get_routes())
        .mount("/category", category::get_routes())
        .attach(cors.to_cors().unwrap())
}
