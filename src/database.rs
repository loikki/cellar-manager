use chrono::Local;
use diesel::mysql::MysqlConnection;
use diesel::Connection;
use diesel::{ExpressionMethods, OptionalExtension, QueryDsl, RunQueryDsl, SelectableHelper};
use diesel_migrations::{embed_migrations, EmbeddedMigrations, MigrationHarness};

use crate::category::Category;
use crate::cellar_error::CellarError;
use crate::item::Item;
use crate::schema::{category, item};

pub struct Database {
    sql: MysqlConnection,
}

impl Database {
    pub fn new(url: &String) -> Self {
        Self {
            sql: MysqlConnection::establish(url).unwrap(),
        }
    }

    pub fn migrate(&mut self) {
        // Migrate DB
        const MIGRATIONS: EmbeddedMigrations = embed_migrations!("migrations/");
        self.sql
            .run_pending_migrations(MIGRATIONS)
            .expect("Failed to run migration");
    }

    // Item
    pub fn get_items(&mut self) -> Vec<Item> {
        item::table
            .select(Item::as_select())
            .load(&mut self.sql)
            .expect("Failed to fetch items")
    }

    pub fn get_item_by_name(&mut self, name: &String) -> Result<Option<Item>, CellarError> {
        let item = item::table
            .select(Item::as_select())
            .filter(item::name.eq(name))
            .first(&mut self.sql)
            .optional()?;
        Ok(item)
    }

    pub fn add_item(&mut self, item: &Item) -> Result<u64, CellarError> {
        diesel::insert_into(item::table)
            .values(item)
            .execute(&mut self.sql)?;
        let item = self.get_item_by_name(&item.name)?;
        Ok(item.unwrap().id.unwrap())
    }

    pub fn set_quantity(&mut self, id: u64, quantity: u32) -> Result<(), CellarError> {
        diesel::update(item::table)
            .filter(item::id.eq(id))
            .set((
                item::quantity.eq(quantity),
                item::last_modification.eq(Local::now().naive_local()),
            ))
            .execute(&mut self.sql)?;
        Ok(())
    }

    pub fn delete_item(&mut self, id: u64) -> Result<(), CellarError> {
        diesel::delete(item::table.filter(item::id.eq(id))).execute(&mut self.sql)?;
        Ok(())
    }

    // Category
    pub fn get_categories(&mut self) -> Vec<Category> {
        category::table
            .select(Category::as_select())
            .load(&mut self.sql)
            .expect("Failed to fetch categories")
    }

    pub fn get_category_by_name(&mut self, name: &String) -> Result<Option<Category>, CellarError> {
        let category = category::table
            .select(Category::as_select())
            .filter(category::name.eq(name))
            .first(&mut self.sql)
            .optional()?;
        Ok(category)
    }

    pub fn add_category(&mut self, category: &Category) -> Result<u64, CellarError> {
        diesel::insert_into(category::table)
            .values(category)
            .execute(&mut self.sql)?;
        let category = self.get_category_by_name(&category.name)?;
        Ok(category.unwrap().id.unwrap())
    }

    pub fn delete_category(&mut self, id: u64) -> Result<(), CellarError> {
        diesel::delete(category::table.filter(category::id.eq(id))).execute(&mut self.sql)?;
        Ok(())
    }
}
