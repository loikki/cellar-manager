#[cfg(feature = "diesel")]
use diesel::deserialize::Queryable;
#[cfg(feature = "diesel")]
use diesel::prelude::Insertable;
#[cfg(feature = "diesel")]
use diesel::{AsChangeset, Selectable};
use serde::{Deserialize, Serialize};

#[cfg_attr(
    feature = "diesel",
    derive(Selectable, Queryable, AsChangeset, Insertable)
)]
#[cfg_attr(feature="diesel", diesel(table_name=crate::schema::category))]
#[derive(Deserialize, Serialize, Clone)]
pub struct Category {
    #[cfg_attr(feature="diesel", diesel(deserialize_as = u64))]
    pub id: Option<u64>,
    pub name: String,
}
