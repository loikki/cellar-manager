use chrono::NaiveDateTime;
#[cfg(feature = "diesel")]
use diesel::deserialize::Queryable;
#[cfg(feature = "diesel")]
use diesel::prelude::Insertable;
#[cfg(feature = "diesel")]
use diesel::{AsChangeset, Selectable};
use serde::{Deserialize, Serialize};

#[cfg_attr(
    feature = "diesel",
    derive(Selectable, Queryable, AsChangeset, Insertable)
)]
#[cfg_attr(feature="diesel", diesel(table_name=crate::schema::item))]
#[derive(Deserialize, Serialize, Clone)]
pub struct Item {
    #[cfg_attr(feature="diesel", diesel(deserialize_as = u64))]
    pub id: Option<u64>,
    pub name: String,
    pub quantity: u32,
    #[cfg_attr(feature="diesel", diesel(deserialize_as = NaiveDateTime))]
    pub last_modification: Option<NaiveDateTime>,
    pub category_id: u64,
}
