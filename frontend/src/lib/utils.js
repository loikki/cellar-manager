import { writable, get } from 'svelte/store';
import { toast } from '@zerodevx/svelte-toast';

const backend = writable(null);

export async function init_backend() {
	// Already init
	if (get(backend) !== null || !window.location.href.includes('localhost')) {
		return;
	}

	// Ask for the backend
	let backend_address = localStorage.getItem('backend_address');
	if (backend_address === null) {
		backend_address = prompt('Please enter the backend server address');
		if (backend_address === null) {
			return;
		}
	}

	localStorage.setItem('backend_address', backend_address);
	backend.set(backend_address);
}

export function get_server() {
	let current = window.location.origin;
	if (current.includes('localhost')) return 'http://localhost:8000';
	else return current;
}

export async function call_backend(method, path, body, error_message, no_answer = false) {
	if (body !== null) {
		body = JSON.stringify(body);
	}
	const response = await fetch(get_server() + path, {
		method: method,
		body: body
	});
	if (!response.ok) {
		let text = await response.text();
		toast.push(`${error_message}: ${text}`);
		return null;
	}
	if (no_answer) {
		return true;
	}
	return await response.json();
}

export async function get_categories() {
	let categories = await call_backend('GET', '/category', null, 'Failed to fetch categories');
	return categories.map((x) => {
		return { value: x.id, name: x.name };
	});
}

export async function get_items() {
	return await call_backend('GET', '/item', null, 'Failed to fetch items');
}
